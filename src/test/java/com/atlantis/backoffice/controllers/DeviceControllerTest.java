package com.atlantis.backoffice.controllers;
/*
import com.atlantis.backoffice.SpringTemplateApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebMvcTest(DeviceController.class)
@TestPropertySource(locations="classpath:test.properties")
@ContextConfiguration(classes = SpringTemplateApplication.class)
@Sql(
    value = "classpath:db/migration/java-db.sql",
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD
)
public class DeviceControllerTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void creation() throws Exception {
        mvc.perform(get("/api/controllers/devices")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}
*/