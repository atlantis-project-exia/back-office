package com.atlantis.backoffice.controllers;

import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;


@RestController
public class GroupController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private DeviceService deviceService;

    @PostMapping("/api/backoffice/groups")
    @PreAuthorize("hasAuthority('TRUE')")
    public Group groups(@RequestBody Map<String, Object> body) {
        Group group = new Group();
        return groupService.createOrUpdate(group, body);
    }

    @GetMapping("/api/backoffice/groups")
    @PreAuthorize("hasAuthority('TRUE')")
    public List<Group> all() {
        return groupService.findAll();
    }

    @GetMapping("/api/backoffice/groups/{id}/devices")
    @PreAuthorize("hasAuthority('TRUE')")
    public List<Device> groupDevices(@PathVariable("id") UUID id) {
        return groupService.getOne(id).getDevices();
    }

    @PostMapping("/api/backoffice/groups/{id}/devices")
    @PreAuthorize("hasAuthority('TRUE')")
    public List<Device> saveGroupDevices(@PathVariable("id") UUID id, @RequestBody List<UUID> body) {
        groupService.getOne(id).getDevices().removeAll(groupService.getOne(id).getDevices());
        groupService.getOne(id).getDevices().addAll(deviceService.findAllById(body));
        groupService.save(groupService.getOne(id));
        return groupService.getOne(id).getDevices();
    }
    @GetMapping("/api/backoffice/groups/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public Group one(@PathVariable("id") UUID id) {
        return groupService.getOne(id);
    }

    @PutMapping("/api/backoffice/groups/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public Group update(@PathVariable("id") UUID id, @RequestBody Map<String, Object> body) {
        Group group = groupService.getOne(id);
        return groupService.createOrUpdate(group, body);
    }

    @DeleteMapping("/api/backoffice/groups/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public void del(@PathVariable("id") UUID id) {
        Group group = groupService.getOne(id);
        groupService.delete(group);
    }
}

