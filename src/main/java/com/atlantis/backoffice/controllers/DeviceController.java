package com.atlantis.backoffice.controllers;


import com.atlantis.backoffice.config.Const;
import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(
        value = {"/api/backoffice"},
        produces = MediaType.APPLICATION_JSON_VALUE
)
@Validated
public class DeviceController {


    @Autowired
    private TokenStore tokenStore;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private GroupService groupService;

    @RequestMapping(method = RequestMethod.GET, path = "/token/list")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('TRUE')")
    public List<String> findAllTokens() {
        final Collection<OAuth2AccessToken> tokensByClientId = tokenStore.findTokensByClientId(Const.CLIENT_ID);

        return tokensByClientId.stream().map(token -> token.getValue()).collect(Collectors.toList());
    }

    @PostMapping("/devices")
    @PreAuthorize("hasAuthority('TRUE')")
    public Device devices(@RequestBody Map<String, Object> body) {
        Device device = new Device();
        return deviceService.createOrUpdate(device, body);
    }

    @GetMapping("/devices")
    @PreAuthorize("hasAuthority('TRUE')")
    public List<Device> all(Principal user) {
        if (user instanceof OAuth2Authentication) {
            System.out.println(((OAuth2Authentication) user).getAuthorities());
        }
        return deviceService.findAll();
    }

    @GetMapping("/devices/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public Device one(@PathVariable("id") UUID id) {
        return deviceService.getOne(id);
    }

    @PutMapping("/devices/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public Device update(@PathVariable("id") UUID id, @RequestBody Map<String, Object> body) {
        Device device = deviceService.getOne(id);
        return deviceService.createOrUpdate(device, body);
    }

    @DeleteMapping("/devices/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public void del(@PathVariable("id") UUID id) {
        Device device = deviceService.getOne(id);
        for (Group group : device.getGroups()) {
            group.getDevices().remove(device);
            groupService.save(group);
        }
    }
}
