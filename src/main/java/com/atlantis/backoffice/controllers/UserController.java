package com.atlantis.backoffice.controllers;


import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private GroupService groupService;

    @PostMapping("/api/backoffice/users")
    @PreAuthorize("hasAuthority('TRUE')")
    public User users(@RequestBody Map<String, Object> body) {
        User user = new User();
        user.setCreated_at(LocalDateTime.now());
        return userService.createOrUpdate(user, body);
    }

    @GetMapping("/api/backoffice/users")
    @PreAuthorize("hasAuthority('TRUE')")
    public List<User> all() {
        return userService.findAll();
    }

    @GetMapping("/api/backoffice/users/{id}/groups")
    @PreAuthorize("hasAuthority('TRUE')")
    public List<Group> userGroups(@PathVariable("id") UUID id) {
        return userService.getOne(id).getGroups();
    }

    @PostMapping("/api/backoffice/users/{id}/groups")
    @PreAuthorize("hasAuthority('TRUE')")
    public List<Group> saveUserGroups(@PathVariable("id") UUID id, @RequestBody List<UUID> body) {
        userService.getOne(id).getGroups().removeAll(userService.getOne(id).getGroups());
        userService.getOne(id).getGroups().addAll(groupService.findAllById(body));
        userService.save(userService.getOne(id));
        return userService.getOne(id).getGroups();
    }

    @GetMapping("/api/backoffice/users/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public User one(@PathVariable("id") UUID id) {
        return userService.getOne(id);
    }

    @PutMapping("/api/backoffice/users/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public User update(@PathVariable("id") UUID id, @RequestBody Map<String, Object> body) {
        User user = userService.getOne(id);
        return userService.createOrUpdate(user, body);
    }

    @DeleteMapping("/api/backoffice/users/{id}")
    @PreAuthorize("hasAuthority('TRUE')")
    public void del(@PathVariable("id") UUID id) {
        User user = userService.getOne(id);
        for (Group group : user.getGroups()) {
            group.getUsers().remove(user);
            groupService.save(group);
        }
        userService.delete(user);
    }
}
