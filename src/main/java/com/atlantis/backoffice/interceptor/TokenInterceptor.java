package com.atlantis.backoffice.interceptor;

import com.atlantis.entities.services.UserService;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenInterceptor implements HandlerInterceptor {
    public TokenInterceptor() { }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        response.addHeader("Access-Control-Allow-Origin", "*");

        return true;
    }
}
