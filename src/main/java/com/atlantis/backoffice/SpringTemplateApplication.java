package com.atlantis.backoffice;


import com.atlantis.backoffice.interceptor.TokenInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication(scanBasePackages = {
        "com.atlantis.backoffice.controllers",
        "com.atlantis.entities.repositories",
        "com.atlantis.entities.services",
        "com.atlantis.backoffice"
}, exclude = {SecurityAutoConfiguration.class})
@EntityScan("com.atlantis.entities.models")
@EnableJpaRepositories("com.atlantis.entities.repositories")
public class SpringTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTemplateApplication.class, args);
    }

}
