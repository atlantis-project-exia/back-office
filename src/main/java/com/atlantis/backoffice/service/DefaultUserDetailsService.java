package com.atlantis.backoffice.service;

import com.atlantis.entities.models.User;
import com.atlantis.entities.repositories.UserRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;
import java.util.Optional;

public class DefaultUserDetailsService implements UserDetailsService {

    private final UserRepository appUserRepository;

    public DefaultUserDetailsService(UserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final Optional<User> userEntity = appUserRepository.findOneByEmail(username);

        if (userEntity.isPresent()) {
            final User appUser = userEntity.get();

            return new org.springframework.security.core.userdetails.User(appUser.getEmail(),
                    passwordNoEncoding(appUser),
                    Collections.singletonList(new SimpleGrantedAuthority(appUser.getIs_admin().toString().toUpperCase())));
        }

        return null;
    }

    private String passwordNoEncoding(User appUser) {
        // you can use one of bcrypt/noop/pbkdf2/scrypt/sha256
        // more: https://spring.io/blog/2017/11/01/spring-security-5-0-0-rc1-released#password-encoding
        return appUser.getPassword();
    }
}
